﻿using System;

namespace Tools.CSV
{
    public class CSVCellInfo
    {
        public CSVColumnInfo ColumnInfo
        {
            get;
            private set;
        }

        public String Value
        {
            get;
            private set;
        }

        public CSVCellInfo( CSVColumnInfo columnInfo, String value )
        {
            this.Value = value;
            this.ColumnInfo = columnInfo;
        }
    }
}
